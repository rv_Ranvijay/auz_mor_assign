package controllers

import javax.inject.Inject
import play.api._
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json
import scala.concurrent.{ ExecutionContext, Future, Promise }
import model._
import dao._
import play.api.cache.SyncCacheApi;
import scala.concurrent.{ Await, Future }
import scala.concurrent.duration._
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.{ Lang, Langs }
import play.filters.csrf._
import play.filters.csrf.CSRF.Token
import org.apache.commons.codec.binary.Base64
import play.api.http._

class APIController @Inject() (
  cc:             ControllerComponents,
  accountDAO:     AccountDao,
  phoneNumberDao: PhoneNumberDao,
  cacheApi:       SyncCacheApi,
  messagesAction: MessagesActionBuilder,
  addToken:       CSRFAddToken, checkToken: CSRFCheck) extends AbstractController(cc) with play.api.i18n.I18nSupport {

  val loginForm = Form(
    mapping(
      "username" -> nonEmptyText(),
      "password" -> nonEmptyText())(User.apply)(User.unapply))

  /**
   * Login page.
   */
  /*def loginLandingPage = messagesAction { implicit request: MessagesRequest[AnyContent] =>
    Ok(views.html.login(loginFormloginForm.fill(User("john","secret@123"))))
  }*/

  /*def simpleFormPost = messagesAction { implicit request: MessagesRequest[AnyContent] =>

    loginForm.bindFromRequest.fold(
        formWithErrors => {
          // binding failure, you retrieve the form containing errors:
            BadRequest(views.html.login(formWithErrors))
        },
        userData => {
           binding success, you get the actual value.
          val newUser = model.User(userData.username, userData.password)
          Redirect(routes.APIController.inBoundSms())
    })

   loginForm.bindFromRequest();

   // val formData  = loginForm.bindFromRequest.get // Careful: BasicForm.form.bindFromRequest returns an Option
   // Ok(formData.toString) // just returning the data because it's an example :)
  }*/

  /*def usercredentailsForm = Action(parse.form(loginForm,
                                      onErrors = (formWithErrors: Form[User]) => {
                                                      implicit val messages = messagesApi.preferred(Seq(Lang.defaultLang))
                                                      BadRequest(views.html.login(formWithErrors))
                                                 })
                              ) { implicit request =>
                                            val userData = request.body
                                            val authentication_result = Await.result(accountDAO.authenticateAccount("username","password"), Duration.Inf)
                                            if(authentication_result.getOrElse(Account(0l,"fake user","fake password")).username.equalsIgnoreCase("fake user")){
                                              Ok("User not exist")
                                            }else{
                                              Ok("user " + authentication_result.get.username + " exists")
                                            //Redirect(routes.APIController.inBoundSms())
                                            }

                                }*/

  def getPhoneNumbers = Action.async { implicit request =>
    phoneNumberDao.list().map { ph_num =>
      Ok(Json.toJson(ph_num))
    }
  }

  def catchAllPath = Action { implicit request =>
    Ok(Json.obj("message" -> "Enter a valid url", "error" -> "Invalid path"))
  }
  
  def NotAlloweed = Action { request =>
    Results.MethodNotAllowed(Json.obj("message" -> "", "error" -> "Only post requests are allowed"))
  }
  /**
   * For debug purpose
   */
  def findUserByID = Action(parse.json) { implicit request =>
    val from = ((request.body \ "from").asOpt[String]).getOrElse("")
    val f = Await.result(phoneNumberDao.findById(1L), Duration.Inf)
    val res = f.filter(_ == "4924195509198").length
    if (res != 0) {
      Ok("Found to")
    } else {
      Ok("Not Found to")
    }

  }

  def inBoundSms = Action(parse.json) { implicit request =>

    if (request.method.equalsIgnoreCase("POST")) { // this is being handled in the customer filter set in appliation.conf refer to the ExampleFilter.scala class
      val from = ((request.body \ "from").asOpt[String]).getOrElse("")
      val to = (request.body \ "to").asOpt[String].getOrElse("")
      val message = (request.body \ "message").asOpt[String].getOrElse("")

      val authorization = request.headers.get("Authorization").getOrElse("without credentials")

      if (authorization != null && authorization.toLowerCase().startsWith("basic")) {
        val base64Credentials = authorization.substring("Basic".length()).trim();
        val credDecoded = Base64.decodeBase64(base64Credentials);
        val credentials = new String(credDecoded).split(":")
        val authentication_result = Await.result(accountDAO.authenticateAccount(credentials(0), credentials(1)), Duration.Inf)
        if (authentication_result.getOrElse(Account(0l, "fake password", "fake user")).username.equalsIgnoreCase("fake user")) {
          Ok(Json.obj("message" -> "Invalid username/password", "error" -> "User doesn't exists"))
        } else {
          try {

            val loggedInUserId = authentication_result.get.id
            println("logged user id " + loggedInUserId)
            if (from.length == 0) {
              Ok(Json.obj("message" -> "", "error" -> "from is missing"))
            } else if (to.length() == 0) {
              Ok(Json.obj("message" -> "", "error" -> "to is missing"))
            } else if (message.length() == 0) {
              Ok(Json.obj("message" -> "", "error" -> "message is missing"))
            } else if ((from.length() < 6) || (from.length() > 16)) {
              Ok(Json.obj("message" -> "", "error" -> "from parameter is invalid "))
            } else if ((to.length() < 6) || (to.length() > 16)) {
              Ok(Json.obj("message" -> "", "error" -> "to parameter is invalid "))
            } else if ((message.length() < 1) || (message.length() > 120)) {
              Ok(Json.obj("message" -> "", "error" -> "from parameter is invalid "))
            } else if (("[aA-zZ]".r.findAllIn(from).toList.length > 0)) {
              Ok(Json.obj("message" -> "", "error" -> "from parameter is invalid "))
            } else if (("[aA-zZ]".r.findAllIn(to).toList.length > 0)) {
              Ok(Json.obj("message" -> "", "error" -> "to parameter is invalid "))
            } else {

              try {
                val loggedInUserIdExists = checkIfKeyExists(NamedConstants.LOGGED_IN_USER_ID, cacheApi)
                val timeStampForFromUser = checkIfKeyExists(NamedConstants.TIME_STAMP_KEY_FOR_FROM_ID_PAIR+from.toString() + "_" + loggedInUserId, cacheApi)

                if (timeStampForFromUser.equals("-1")) {
                  //Await.result(cacheApi.set(from.toString() + "_" + loggedInUserId, System.currentTimeMillis(), 14400.minute), Duration.Inf)
                  cacheApi.set(NamedConstants.TIME_STAMP_KEY_FOR_FROM_ID_PAIR+from.toString() + "_" + loggedInUserId, System.currentTimeMillis(), 14400.minute)
                }

                if (loggedInUserIdExists.equals("-1")) {
                  //Await.result(cacheApi.set("LoggedInUserId", loggedInUserId, 240.minute), Duration.Inf)
                  cacheApi.set(NamedConstants.LOGGED_IN_USER_ID, loggedInUserId, 240.minute)
                }
                println("logged" + cacheApi.get(NamedConstants.TIME_STAMP_KEY_FOR_FROM_ID_PAIR+from.toString() + "_" + loggedInUserId))
              } catch {
                case ex: NoSuchElementException => Ok(Json.obj("message" -> "Error in checkIfKeyExists", "error" -> "User doesn't exist"))
                case ex: Exception              => Ok(Json.obj("message" -> "Error in checkIfKeyExists", "error" -> s"""Unknown Exceptions"""))
              }

              val is_number_present_for_the_to_parameter = Await.result(phoneNumberDao.findById(loggedInUserId), Duration.Inf).filter(_.toString() == to).length
              if (is_number_present_for_the_to_parameter == 0) {
                Ok(Json.obj("message" -> "", "error" -> "to parameter not found", "list" -> is_number_present_for_the_to_parameter.toString()))
              } else if (message.filter(_ >= ' ').toString().equalsIgnoreCase("stop")) {
                if (checkIfKeyExists(from.toString()+"_"+to.toString(), cacheApi).equals("-1")) {
                  //Await.result(cacheApi.set(from, to, 240.minute), Duration.Inf) // set from to in cache for 4 hours
                  cacheApi.set(from.toString()+"_"+to.toString(), to, 240.minute)
                }
                Ok(Json.obj("message" -> "InBound SMS ok with stop", "error" -> "", "cache" -> cacheApi.get(from).toString()))
              } else {
                Ok(Json.obj("message" -> "InBound SMS ok", "error" -> "", "cahe" -> cacheApi.get("LoggedInUserId").toString()))
              }
            }

          } catch {

            case ex: NoSuchElementException => Ok(Json.obj("message" -> "Invalid username/password", "error" -> "User doesn't exist"))
            case ex: Exception              => Ok(Json.obj("message" -> "Invalid username/password", "error" -> s"""Unknown Exceptions"""))
          }
        }
      } else {
        Ok(Json.obj("message" -> "Provide credentials", "error" -> ""))
      }

    } else {
      MethodNotAllowed(Json.obj("message" -> "", "error" -> "Only post requests are allowed"))
    }
  }

  def outBoundSms = Action(parse.json) { implicit request =>
    if (request.method.equalsIgnoreCase("POST")) { // this is being handled in the customer filter set in appliation.conf refer to the ExampleFilter.scala class
      val from = ((request.body \ "from").asOpt[String]).getOrElse("")
      val to = (request.body \ "to").asOpt[String].getOrElse("")
      val message = (request.body \ "message").asOpt[String].getOrElse("")

      val authorization = request.headers.get("Authorization").getOrElse("without credentials")

      if (authorization != null && authorization.toLowerCase().startsWith("basic")) {
        val base64Credentials = authorization.substring("Basic".length()).trim();
        val credDecoded = Base64.decodeBase64(base64Credentials);
        val credentials = new String(credDecoded).split(":")
        val authentication_result = Await.result(accountDAO.authenticateAccount(credentials(0), credentials(1)), Duration.Inf)
        
        if (authentication_result.getOrElse(Account(0l, "fake password", "dummy user")).username.equalsIgnoreCase("dummy user")) {
          Ok(Json.obj("message" -> "Invalid username/password", "error" -> "User doesn't exists"))
        } else {
          try {
          /*var loggedInUserId = 0l
          try {
            loggedInUserId = authentication_result.get.id
          } catch {
            case ex: NoSuchElementException => Ok(Json.obj("message" -> "Error in checkIfKeyExists", "error" -> "UloggedInUserId not resolved"))
            case ex: Exception              => Ok(Json.obj("message" -> "Error in checkIfKeyExists", "error" -> s"""Unknown Exceptions """))
          }*/
            
           val loggedInUserId = authentication_result.get.id
           
           println("logged user id " + loggedInUserId)
            
          if (from.length == 0) {
            Ok(Json.obj("message" -> "", "error" -> "from is missing"))
          } else if (to.length() == 0) {
            Ok(Json.obj("message" -> "", "error" -> "to is missing"))
          } else if (message.length() == 0) {
            Ok(Json.obj("message" -> "", "error" -> "message is missing"))
          } else if ((from.length() < 6) || (from.length() > 16)) {
            Ok(Json.obj("message" -> "", "error" -> "from parameter is invalid "))
          } else if ((to.length() < 6) || (to.length() > 16)) {
            Ok(Json.obj("message" -> "", "error" -> "to parameter is invalid "))
          } else if ((message.length() < 1) || (message.length() > 120)) {
            Ok(Json.obj("message" -> "", "error" -> "from parameter is invalid "))
          } else if (("[aA-zZ]".r.findAllIn(from).toList.length > 0)) {
            Ok(Json.obj("message" -> "", "error" -> "from parameter is invalid "))
          } else if (("[aA-zZ]".r.findAllIn(to).toList.length > 0)) {
            Ok(Json.obj("message" -> "", "error" -> "to parameter is invalid "))
          } else {
            //try {
              val loggedInUserIdExists = checkIfKeyExists(NamedConstants.LOGGED_IN_USER_ID, cacheApi)
              val timeStampForFromUser = checkIfKeyExists(NamedConstants.TIME_STAMP_KEY_FOR_FROM_ID_PAIR+from.toString() + "_" + loggedInUserId, cacheApi)
              val numOfRequestForAUser = checkIfKeyExists(NamedConstants.NUM_OF_REQUEST_FROM + from, cacheApi)

              println(loggedInUserIdExists)
              
              if (loggedInUserIdExists.equals("-1")) {
                cacheApi.set(NamedConstants.LOGGED_IN_USER_ID, loggedInUserId, 240.minute)
                println( "logged in user id  "+ loggedInUserId + " being added in cache")
              }

              println(timeStampForFromUser)
              
              if (timeStampForFromUser.equals("-1")) {
                println( "time stamp for  "+ from.toString() + "_" + loggedInUserId + " being added in cache")
                cacheApi.set(NamedConstants.TIME_STAMP_KEY_FOR_FROM_ID_PAIR+from.toString() + "_" + loggedInUserId, System.currentTimeMillis(), 14400.minute)
              }
              
              println(numOfRequestForAUser)
              
              if (numOfRequestForAUser.equals("-1")) {
                //Await.result(cacheApi.set("num_of_request_from_" + from, "0"), Duration.Inf)
                println( NamedConstants.NUM_OF_REQUEST_FROM+ from + " being added in cache")
                cacheApi.set(NamedConstants.NUM_OF_REQUEST_FROM + from, "0")
              }
              
             // println("num_of_request_from_" + from)
           
            /*} catch {
              case ex: NoSuchElementException => Ok(Json.obj("message" -> "Error in checkIfKeyExists", "error" -> "User doesn't exist", "cache" -> cacheApi.get(NamedConstants.LOGGED_IN_USER_ID).toString()))
              case ex: Exception              => Ok(Json.obj("message" -> "Error in checkIfKeyExists", "error" -> s"""Unknown Exceptions $authentication_result.get.id """))
            }*/

            val is_from_exist_in_cache = checkIfKeyExists(from.toString()+"_"+to.toString(), cacheApi)
            if (!(is_from_exist_in_cache == to)) { // if 'from' 'to' does not exist in the cache
              val logged_in_user_id = cacheApi.get[Long](NamedConstants.LOGGED_IN_USER_ID)
              val is_number_present_for_the_from_parameter = Await.result(phoneNumberDao.findById(loggedInUserId.toLong), Duration.Inf).filter(_.toString() == from).length
              if (is_number_present_for_the_from_parameter == 0) {
                Ok(Json.obj("message" -> "", "error" -> "from parameter not found", "list" -> is_number_present_for_the_from_parameter.toString()))
              } else if ((getNumberOfRequestFromAUser(NamedConstants.NUM_OF_REQUEST_FROM + from, cacheApi) < 50l) && (checkIfWithinTimeLimit(NamedConstants.TIME_STAMP_KEY_FOR_FROM_ID_PAIR+from.toString() + "_" + loggedInUserId, cacheApi,/*converted 24 hrs into millisecond*/ 86400000l))) {
               // println(getNumberOfRequestFromAUser("num_of_request_from_" + from, cacheApi))
                updateNumberOfRequestFromAUser(NamedConstants.NUM_OF_REQUEST_FROM + from, cacheApi)
                Ok(Json.obj("message" -> "outbound sms ok", "error" -> ""))

              } else {
                resetNumberOfRequestForAUser(NamedConstants.TIME_STAMP_KEY_FOR_FROM_ID_PAIR+from, cacheApi)
                Ok(Json.obj("message" -> "", "error" -> s"""time limit reached for $from"""))

              }
            } else { // else post message that sms from has been blocked
              Ok(Json.obj("message" -> "", "error" -> s"""sms from  $from to $to blocked by STOP request"""))
            }

          }

           } catch {

            case ex: NoSuchElementException => Ok(Json.obj("message" -> s"""Invalid username/password""","error" -> "User doesn't exist"))
            case ex: Exception              => Ok(Json.obj("message" -> "Invalid username/password", "error" -> s"""Unknown Exceptions """))
          }
        }
      } else {
        Ok(Json.obj("message" -> "Provide credentials", "error" -> ""))
      }

    } else {
      MethodNotAllowed(Json.obj("message" -> "", "error" -> "Only post requests are allowed"))
    }
  }

  def checkIfKeyExists(key: String, cacheApi: SyncCacheApi) =  {
   // Await.result(cacheApi.get(key), Duration.Inf) 
    cacheApi.get[String](key) match {
      case Some(x) => x.toString()
      case None    => "-1".toString() // or other code to handle an expired key
    }
  }

  def getNumberOfRequestFromAUser(key: String, cacheApi: SyncCacheApi): Long = synchronized {
    println("entry for " + key + " is being fetched from cache")
   // Await.result(cacheApi.get[Long](key), Duration.Inf).getOrElse(0l)
    cacheApi.get[String](key).get.toLong
  }

  def resetNumberOfRequestForAUser(key: String, cacheApi: SyncCacheApi) = synchronized {
   // Await.result(cacheApi.set(key, 0), Duration.Inf)
    cacheApi.set(key, 0)
  }

  def updateNumberOfRequestFromAUser(from: String, cacheApi: SyncCacheApi) = synchronized {
    val current_count = cacheApi.get[String](from).get.toLong // Await.result(cacheApi.get[String](from), Duration.Inf).get.toLong
   // Await.result(cacheApi.set(from, (current_count + 1).toString()), Duration.Inf)
    cacheApi.set(from, (current_count + 1).toString())
  }

  def checkIfWithinTimeLimit(key: String, cacheApi: SyncCacheApi, allowedTimeLimit: Long) = synchronized {
    val isWithinTimeLimit = false
    val logged_time_stamp_in_milli_sec = cacheApi.get[Long](key).get//Await.result(cacheApi.get[Long](key), Duration.Inf).get
    val current_time_stamp = System.currentTimeMillis()
    if ((current_time_stamp - logged_time_stamp_in_milli_sec) > allowedTimeLimit) {
      isWithinTimeLimit
    } else {
      !isWithinTimeLimit
    }
  }

}