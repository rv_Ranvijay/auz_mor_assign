package dao


import javax.inject.Inject
import scala.concurrent.{ Future, ExecutionContext }
import model.{PhoneNumber,Account}
import javax.inject.Singleton
import play.api.db.slick.{HasDatabaseConfigProvider, DatabaseConfigProvider}
import slick.jdbc.{ JdbcProfile, PostgresProfile }
import slick.lifted.Tag
import slick.jdbc.PostgresProfile.api._

@Singleton
class PhoneNumberDao @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) { 
    
  // We want the JdbcProfile for this provider
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  // These imports are important, the first one brings db into scope, which will let you do the actual db operations.
  // The second one brings the Slick DSL into scope, which lets you define the table and other queries.
  import dbConfig._
  import profile.api
  
  
  
  class PhoneNumberFRM(tag: Tag) extends Table[PhoneNumber](tag, "phone_number") {

    def id = column[Long]("id")
    def number = column[String]("number",O.PrimaryKey)
    def account_id = column[Long]("account_id")

    def * = (id, number, account_id) <> ((PhoneNumber.apply _).tupled,PhoneNumber.unapply _)
  }
  
  private val phoneNumbers = TableQuery[PhoneNumberFRM]
  
  def findById(id: Long): Future[Seq[String]] =
    db.run(phoneNumbers.filter(_.account_id === id).map(_.number).result)
    
  def list(): Future[Seq[PhoneNumber]] = db.run {
    phoneNumbers.result
  }
  
  
  
  
  
}