package dao


import javax.inject.Inject
import scala.concurrent.{ Future, ExecutionContext }
import model.{PhoneNumber,Account}
import javax.inject.Singleton
import play.api.db.slick.{HasDatabaseConfigProvider, DatabaseConfigProvider}
import slick.jdbc.{ JdbcProfile, PostgresProfile }
import slick.lifted.Tag
import slick.jdbc.PostgresProfile.api._


class AccountDao @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) { 
    // We want the JdbcProfile for this provider
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  // These imports are important, the first one brings db into scope, which will let you do the actual db operations.
  // The second one brings the Slick DSL into scope, which lets you define the table and other queries.
  import dbConfig._
  import profile.api 
  
  
  class AccountFRM(tag: Tag) extends Table[Account](tag, "account") {

    def id = column[Long]("id")
    def auth_id = column[String]("auth_id",O.PrimaryKey)
    def user_name = column[String]("username")

    def * = (id, auth_id, user_name) <> ((Account.apply _).tupled,Account.unapply _)
  }
  
  private val accountTable = TableQuery[AccountFRM]
  
  def findById(username: String, password: String): Future[Option[Account]] =
    db.run(accountTable.filter(rec => (rec.user_name===username) && (rec.auth_id===password) ).result.headOption)
  
  def isUserPresent(username: String, password: String):Future[Seq[Account]] = db.run {
      accountTable.result
  }
  
  def authenticateAccount(username: String, password: String): Future[Option[Account]] =
    db.run(accountTable.filter(rec => (rec.user_name===username) && (rec.auth_id===password) ).result.headOption)
  
}