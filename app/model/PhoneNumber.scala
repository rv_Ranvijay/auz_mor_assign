package model

import play.api.libs.json._

case class PhoneNumber(id: Long, number: String, account_id: Long)

object PhoneNumber {  
  implicit val PhoneNumber = Json.format[PhoneNumber]
}
