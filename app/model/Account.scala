package model

import play.api.libs.json._

case class Account(id: Long, auth_id: String, username: String)

object Account {  
  implicit val Account = Json.format[Account]
}