package filters

import javax.inject._
import play.api.mvc._
import scala.concurrent.ExecutionContext
import play.api.libs.json.Json
import play.api.http._
import play.api._
import play.api.mvc._
import play.api.mvc.{ Request, Result, Results }
import scala.concurrent.{ Await, Future }

import play.api.data.Forms._
import play.api.data._
import play.api.mvc._

/**
 * This is a simple filter that adds a header to all requests. It's
 * added to the application's list of filters by the
 * [[Filters]] class.
 *
 * @param ec This class is needed to execute code asynchronously.
 * It is used below by the `map` method.
 */
@Singleton
class CustomFilter @Inject()(implicit ec: ExecutionContext,cc:ControllerComponents) extends EssentialFilter {
  /* override def apply(next: EssentialAction) = EssentialAction { request =>
    next(request).map { result =>
      result.withHeaders("X-ExampleFilter" -> "foo")
    }
  }*/

  override def apply(next: EssentialAction) = EssentialAction { implicit request =>
    (request.method, request.path) match {
      case ("POST", "/inbound/sms")  => next(request)
      case ("POST", "/outbound/sms") => next(request)
      case _ => NotAllowed(request)
    }
  }
  
  def NotAllowed = Action { request =>
    Results.MethodNotAllowed(Json.obj("message" -> "", "error" -> "only post requests with url /inbound/sms and /outbound/sms are allowed"))
  }
  
}